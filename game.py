from random import randint

player = input("what is your name?")

for guess_number in range(1,6):
    month = randint(1, 12)
    year = randint(1924, 2004)

    print("Guess", guess_number, ":", player + " were you born in ", + month, "/", + year, "?")

    answer = input("yes or no?")

    if answer == "yes":
        print("i knew it!")
        exit()
    elif guess_number == 5:
        print("i have better things to do, cya!")
    else:
        print("let me try again")